from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from meubles.views import MeubleViewSet, MagasinViewSet, LeaderViewSet

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

router = routers.SimpleRouter()

router.register("meubles", MeubleViewSet, basename="meuble")
router.register("leaders", LeaderViewSet, basename="leader")
router.register("magasins", MagasinViewSet, basename="magasin")

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api-auth/", include('rest_framework.urls')),
    path("api/", include(router.urls)),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
