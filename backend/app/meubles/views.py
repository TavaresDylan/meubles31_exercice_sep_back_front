from rest_framework.viewsets import ModelViewSet
from .serializers import MeubleSerializer, LeaderSerializer, MagasinSerializer
from .models import Meuble, Magasin, Leader


class MeubleViewSet(ModelViewSet):
    serializer_class = MeubleSerializer

    def get_queryset(self):
        return Meuble.objects.all()

class LeaderViewSet(ModelViewSet):
    serializer_class = LeaderSerializer

    def get_queryset(self):
        return Leader.objects.all()

class MagasinViewSet(ModelViewSet):
    serializer_class = MagasinSerializer

    def get_queryset(self):
        return Magasin.object.all()
