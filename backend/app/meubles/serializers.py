from rest_framework import serializers
from .models import Meuble, Magasin, Leader

class MeubleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meuble
        fields = "__all__"

class MagasinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Magasin
        fields = "__all__"

class LeaderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Leader
        fields = "__all__"
