from django.contrib import admin
from .models import Meuble, Magasin, Leader

class MeubleAdmin(admin.ModelAdmin):
    list_display = ["id", "name", "state", "price", "storage_location", "sale_state"]

admin.site.register(Meuble, MeubleAdmin)

class MagasinAdmin(admin.ModelAdmin):
    list_display = ["id", "name", "address","leader", "sales_revenue"]

admin.site.register(Magasin, MagasinAdmin)

class LeaderAdmin(admin.ModelAdmin):
    list_display = ["id", "firstname", "lastname"]

admin.site.register(Leader, LeaderAdmin)
