# Generated by Django 4.1.5 on 2023-02-10 08:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meubles', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='meuble',
            name='sale_state',
            field=models.TextField(choices=[('SOLD', 'sold'), ('AVAILABLE', 'available')], default='AVAILABLE'),
        ),
    ]
