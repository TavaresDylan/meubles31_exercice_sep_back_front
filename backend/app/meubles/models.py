from django.db import models

class Leader(models.Model):
    firstname = models.TextField(max_length=120)
    lastname = models.TextField(max_length=120)

    def __str__(self):
        return self.lastname

class Magasin(models.Model):
    name = models.TextField(max_length=120)
    address = models.TextField(max_length=140)
    leader = models.ForeignKey(Leader, on_delete=models.CASCADE)
    sales_revenue = models.IntegerField()

    def __str__(self):
        return self.name

class Meuble(models.Model):
    STATES = [
        ("UNUSED", "unused"),
        ("USED", "used"),
        ("RUINED", "ruined"),
        ("UNUSABLE", "unusable"),
    ]
    SALE_STATE = [
        ("SOLD", "sold"),
        ("AVAILABLE", "available")
    ]
    name = models.TextField(max_length=140)
    state = models.TextField(choices=STATES)
    storage_location = models.ForeignKey(Magasin, on_delete=models.CASCADE)
    sale_state = models.TextField(choices=SALE_STATE, default="AVAILABLE")
    price = models.FloatField(max_length=120)
    
    def __str__(self):
        return self.name