import { createRouter, createWebHistory } from "vue-router";
import Home from "../components/Home.vue";
import Signup from "../views/Signup.vue";
import Login from "../views/Login.vue";
import Notfound from "../views/NotFound.vue";

const routes = [
  {
    path: "/",
    component: Home,
  },
  {
    path: "/login",
    component: Login,
  },
  {
    path: "/signup",
    component: Signup,
  },
  {
    path: "/page-not-found",
    component: Notfound,
  },
  { path: "/:pathMatch(.*)*", name: "page-not-found", component: Notfound },
];

const router = createRouter({
  history: createWebHistory(),
  base: "/",
  routes,
});

router.resolve({
  name: "page-not-found",
  params: { pathMatch: ["page", "not", "found"] },
}).href;

export default router;
